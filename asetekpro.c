// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Corsair asetekpro-based liquid cooler driver
 *
 * Copyright (c) 2020 Harry Austen
 *
 * This driver is inspired by the OpenCorsairLink userspace
 * utility: Copyright (c) 2017-2019 Sean Nelson <audiohacked@gmail.com>
 * and attempts to accumulate functionality similar to that provided by
 * Corsair's iCUE software into the kernel.
 */

// device functions, structs & macros
#include <linux/device.h>
// device logging functions
#include <linux/dev_printk.h>
// gfp macros
#include <linux/gfp.h>
// hwmon functions, macros & structs
#include <linux/hwmon.h>
// kstrtou32 & sprintf
#include <linux/kernel.h>
// module macros
#include <linux/module.h>
// kzalloc & kfree
#include <linux/slab.h>
// attribute struct & macros
#include <linux/sysfs.h>
// usb functions, macros & structs
#include <linux/usb.h>

// Common vendor ID
#define USB_VENDOR_ID_CORSAIR 0x1b1c

// Cooler variant product IDs
#define USB_PRODUCT_ID_H150I_PRO 0x0c12
#define USB_PRODUCT_ID_H115I_PRO 0x0c13
#define USB_PRODUCT_ID_H110I_PRO 0x0c14
#define USB_PRODUCT_ID_H100I_PRO 0x0c15
#define USB_PRODUCT_ID_H80I_PRO  0x0c16

// LED speed settings
#define LED_SPEED_BLINK_FAST	0x05
#define LED_SPEED_BLINK_MED	0x0a
#define LED_SPEED_BLINK_SLOW	0x0f
#define LED_SPEED_RAINBOW_FAST	0x0c
#define LED_SPEED_RAINBOW_MED	0x18
#define LED_SPEED_RAINBOW_SLOW	0x30
#define LED_SPEED_SHIFT_FAST	0x0f
#define LED_SPEED_SHIFT_MED	0x28
#define LED_SPEED_SHIFT_SLOW	0x46
#define LED_SPEED_PULSE_FAST	0x1e
#define LED_SPEED_PULSE_MED	0x37
#define LED_SPEED_PULSE_SLOW	0x50

// Bulk USB commands
#define CMD_READ_PUMP_SPEED	0x31
#define CMD_WRITE_PUMP_MODE	0x32
#define CMD_READ_PUMP_MODE	0x33
#define CMD_WRITE_FAN_CURVE	0x40
#define CMD_READ_FAN_SPEED	0x41
#define CMD_WRITE_FAN_SPEED	0x42
#define CMD_LED_PULSE		0x52
#define CMD_LED_SET_SPEED	0x53
// Depending on if and which speed is sent before this cmd, it either acts as
// rainbow, shift or static
#define CMD_LED_MULTI		0x55
#define CMD_LED_SET_COLOURS	0x56
#define CMD_LED_BLINK		0x58
#define CMD_LED_ALERT		0x5e
#define CMD_LED_SET_TEMPS	0x5f
#define CMD_READ_LIQUID_TEMP	0xa9
#define CMD_READ_FW_VERSION	0xaa

// Control USB request/commands
#define USBXPRESS_REQUEST	0x02
#define USBXPRESS_UNLOCK	0x02
#define USBXPRESS_LOCK		0x04

enum led_mode {
	LED_MODE_FIXED,
	LED_MODE_SHIFT,
	LED_MODE_RAINBOW,
	LED_MODE_PULSE,
	LED_MODE_BLINK,
	LED_MODE_ALERT
};

static const char * const pump_modes[] = {
	"quiet",
	"balanced",
	"performance",
};

static const struct usb_device_id id_table[] = {
	{ USB_DEVICE(USB_VENDOR_ID_CORSAIR, USB_PRODUCT_ID_H150I_PRO) },
	{ USB_DEVICE(USB_VENDOR_ID_CORSAIR, USB_PRODUCT_ID_H115I_PRO) },
	{ USB_DEVICE(USB_VENDOR_ID_CORSAIR, USB_PRODUCT_ID_H110I_PRO) },
	{ USB_DEVICE(USB_VENDOR_ID_CORSAIR, USB_PRODUCT_ID_H100I_PRO) },
	{ USB_DEVICE(USB_VENDOR_ID_CORSAIR, USB_PRODUCT_ID_H80I_PRO)  },
	{ /* sentinel */ },
};
MODULE_DEVICE_TABLE(usb, id_table);

struct asetekpro_device {
	struct usb_device *udev;
	struct device *hdev;
	struct device *dev;
	u8 *buf;

	u8 red;
	u8 green;
	u8 blue;

	u8 in_addr;
	u8 out_addr;
};

static inline struct asetekpro_device *to_adev(struct device *dev)
{
	struct usb_interface *intf = to_usb_interface(dev);

	return usb_get_intfdata(intf);
}

static int asetekpro_control_msg(struct asetekpro_device *adev, u16 value, int timeout)
{
	struct usb_device *udev = adev->udev;
	int ret;

	ret = usb_control_msg(udev, usb_sndctrlpipe(udev, 0), 2,
			      USB_DIR_OUT | USB_TYPE_VENDOR | USB_RECIP_DEVICE,
			      value, 0, NULL, 0, timeout);
	dev_dbg(adev->dev,
		"Control MSG (value = %u, timeout = %d) returned %d\n",
		value, timeout, ret);

	return ret;
}

static int asetekpro_bulk_msg_out(struct asetekpro_device *adev, int len, ...)
{
	struct usb_device *udev = adev->udev;
	int bytes_transferred;
	u8 *buf = adev->buf;
	va_list args;
	int ret, i;

	va_start(args, len);
	for (i = 0; i < len; ++i)
		buf[i] = (u8)va_arg(args, int);
	va_end(args);

	ret = usb_bulk_msg(udev, usb_sndbulkpipe(udev, adev->out_addr),
			   buf, len, &bytes_transferred, 1000);
	if (ret)
		return ret;

	return (bytes_transferred == len) ? 0 : -EIO;
}

static int asetekpro_bulk_msg_in(struct asetekpro_device *adev, int len)
{
	struct usb_device *udev = adev->udev;
	int bytes_transferred;
	int ret;

	ret = usb_bulk_msg(udev, usb_rcvbulkpipe(udev, adev->in_addr),
			   adev->buf, len, &bytes_transferred, 1000);
	dev_dbg(adev->dev,
		"Bulk in MSG (transferred %d/%d bytes) returned %d\n",
		bytes_transferred, len, ret);

	return ret;
}

static bool invalid_response(struct asetekpro_device *adev, u8 cmd)
{
	u8 *buf = adev->buf;

	return buf[0] != cmd || buf[1] != 0x12 || buf[2] != 0x34;
}

static ssize_t version_show(struct device *dev, struct device_attribute *attr, char *user_buf)
{
	struct asetekpro_device *adev = to_adev(dev);
	u8 *buf = adev->buf;
	int ret;

	ret = asetekpro_bulk_msg_out(adev, 1, CMD_READ_FW_VERSION);
	if (ret)
		return ret;

	ret = asetekpro_bulk_msg_in(adev, 7);
	if (ret)
		return ret;

	if (invalid_response(adev, CMD_READ_FW_VERSION)) {
		dev_err(adev->dev, "Invalid FW version response: %02X:%02X:%02X:%02X:%02X:%02X:%02X\n",
			buf[0], buf[1], buf[2], buf[3], buf[4], buf[5], buf[6]);
		return -EIO;
	}

	return sysfs_emit(user_buf, "%u.%u.%u.%u\n", buf[3], buf[4], buf[5], buf[6]);
}
static DEVICE_ATTR_RO(version);

static int asetekpro_set_leds(struct asetekpro_device *adev, enum led_mode mode,
			      u32 colour1, u32 colour2, u8 speed)
{
	u8 r1 = (colour1 & 0xFF0000) >> 16,	r2 = (colour2 & 0xFF0000) >> 16;
	u8 g1 = (colour1 & 0xFF00) >> 8,	g2 = (colour2 & 0xFF00) >> 8;
	u8 b1 = colour1 & 0xFF,			b2 = colour2 & 0xFF;
	u8 *buf = adev->buf;
	int ret;

	ret = asetekpro_bulk_msg_out(adev, 8, CMD_LED_SET_COLOURS, 2, r1, g1, b1, r2, g2, b2);
	if (ret)
		return ret;

	// 3 byte response
	ret = asetekpro_bulk_msg_in(adev, 3);
	if (ret)
		return ret;

	if (invalid_response(adev, CMD_LED_SET_COLOURS)) {
		dev_err(adev->dev, "Setting colours returned: %02X:%02X:%02X\n",
			buf[0], buf[1], buf[2]);
		return -EIO;
	}

	if (mode == LED_MODE_SHIFT) {
		ret = asetekpro_bulk_msg_out(adev, 2, CMD_LED_SET_SPEED, speed);
		if (ret)
			return ret;

		ret = asetekpro_bulk_msg_in(adev, 3);
		if (ret)
			return ret;

		if (invalid_response(adev, CMD_LED_SET_SPEED)) {
			dev_err(adev->dev, "Setting speed returned: %02X:%02X:%02X\n",
				buf[0], buf[1], buf[2]);
			return -EIO;
		}
	}

	ret = asetekpro_bulk_msg_out(adev, 2, CMD_LED_MULTI, 1);
	if (ret)
		return ret;

	// 3 bytes response
	ret = asetekpro_bulk_msg_in(adev, 3);
	if (ret)
		return ret;

	if (invalid_response(adev, CMD_LED_MULTI)) {
		dev_err(adev->dev, "Setting static returned: %02X:%02X:%02X\n",
			buf[0], buf[1], buf[2]);
		return -EIO;
	}

	return 0;
}

static ssize_t fixed_store(struct device *dev, struct device_attribute *attr,
			    const char *buf, size_t count)
{
	struct asetekpro_device *adev = to_adev(dev);
	u32 colour;
	int ret;

	ret = kstrtou32(buf, 0, &colour);
	if (ret)
		return ret;

	if (colour > 0xFFFFFF)
		return -EINVAL;

	ret = asetekpro_set_leds(adev, LED_MODE_FIXED, colour, colour, 0);
	if (ret)
		return ret;

	return (ssize_t)count;
}
static DEVICE_ATTR_WO(fixed);

static int get_colours(const char *buf, u32 *colours, char *speed)
{
	char *dup, *beg, *end;
	int i, ret;

	dup = kstrdup(buf, GFP_KERNEL);
	if (!dup)
		return -ENOMEM;

	end = dup;

	for (i = 0; i < 2; ++i) {
		beg = strsep(&end, " ");
		if (!end) {
			ret = -EINVAL;
			goto free_string;
		}

		ret = kstrtou32(beg, 0, &colours[i]);
		if (ret)
			goto free_string;
	}

	*speed = *end;

free_string:
	kfree(dup);
	return ret;
}

static ssize_t shift_store(struct device *dev, struct device_attribute *attr,
			   const char *buf, size_t count)
{
	struct asetekpro_device *adev = to_adev(dev);
	u32 colours[2];
	u8 speed;
	int ret;
	char s;

	// parse colours from user buf
	ret = get_colours(buf, colours, &s);
	if (ret) {
		dev_err(adev->dev, "Couldn't get colours\n");
		return ret;
	}

	// shift speed
	switch (s) {
	case 'f':
		speed = LED_SPEED_SHIFT_FAST;
		break;
	case 'm':
		speed = LED_SPEED_SHIFT_MED;
		break;
	case 's':
		speed = LED_SPEED_SHIFT_SLOW;
		break;
	default:
		dev_err(adev->dev, "Invalid speed character %c\n", s);
		return -EINVAL;
	}

	ret = asetekpro_set_leds(adev, LED_MODE_SHIFT, colours[0], colours[1], speed);
	if (ret)
		return ret;

	return (ssize_t)count;
}
static DEVICE_ATTR_WO(shift);

static ssize_t pump_mode_show(struct device *dev, struct device_attribute *attr, char *user_buf)
{
	struct asetekpro_device *adev = to_adev(dev);
	u8 *buf = adev->buf;
	int ret;

	ret = asetekpro_bulk_msg_out(adev, 1, CMD_READ_PUMP_MODE);
	if (ret)
		return ret;

	ret = asetekpro_bulk_msg_in(adev, 4);
	if (ret)
		return ret;

	if (invalid_response(adev, CMD_READ_PUMP_MODE) ||
	    buf[3] > sizeof(pump_modes) - 1) {
		dev_err(adev->dev, "Unknown read pump mode response: %02X:%02X:%02X:%02X\n",
			buf[0], buf[1], buf[2], buf[3]);
		return -EIO;
	}

	return sysfs_emit(user_buf, "%s\n", pump_modes[buf[3]]);
}
static DEVICE_ATTR_RO(pump_mode);

static int read_liquid_temp(struct asetekpro_device *adev, long *val)
{
	u8 *buf = adev->buf;
	int ret;

	ret = asetekpro_bulk_msg_out(adev, 1, CMD_READ_LIQUID_TEMP);
	if (ret)
		return ret;

	ret = asetekpro_bulk_msg_in(adev, 5);
	if (ret)
		return ret;

	if (invalid_response(adev, CMD_READ_LIQUID_TEMP)) {
		dev_err(adev->dev, "Unknown read temp response: %02X:%02X:%02X:%02X:%02X\n",
			buf[0], buf[1], buf[2], buf[3], buf[4]);
		return -EIO;
	}

	*val = (1000 * buf[3]) + (100 * buf[4]);
	return 0;
}

static int read_fan_speed(struct asetekpro_device *adev, int channel, long *val)
{
	u8 index = (u8)channel;
	u8 *buf = adev->buf;
	int ret;

	if (channel != (int)index)
		return -EINVAL;

	ret = asetekpro_bulk_msg_out(adev, 2, CMD_READ_FAN_SPEED, index);
	if (ret)
		return ret;

	ret = asetekpro_bulk_msg_in(adev, 6);
	if (ret)
		return ret;

	if (invalid_response(adev, CMD_READ_FAN_SPEED) ||
	    buf[3] != index) {
		dev_err(adev->dev, "Unknown read fan speed response: %02X:%02X:%02X:%02X:%02X:%02X\n",
			buf[0], buf[1], buf[2], buf[3], buf[4], buf[5]);
		return -EIO;
	}

	*val = be16_to_cpu(*((__force __be16 *)(&buf[4])));
	return 0;
}

static int asetekpro_hwmon_read(struct device *dev, enum hwmon_sensor_types type,
				u32 attr, int channel, long *val)
{
	struct asetekpro_device *adev = to_adev(dev);

	switch (type) {
	case hwmon_temp:
		switch (attr) {
		case hwmon_temp_input:
			return read_liquid_temp(adev, val);
		default:
			break;
		}
		break;
	case hwmon_fan:
		switch (attr) {
		case hwmon_fan_input:
			return read_fan_speed(adev, channel, val);
		default:
			break;
		}
		break;
	default:
		break;
	}

	return -EOPNOTSUPP;
}

static umode_t asetekpro_hwmon_is_visible(const void *data, enum hwmon_sensor_types type,
					  u32 attr, int channel)
{
	return 0444;
}

static const struct hwmon_channel_info *asetekpro_hwmon_info[] = {
	HWMON_CHANNEL_INFO(temp, HWMON_T_INPUT),
	HWMON_CHANNEL_INFO(fan, HWMON_F_INPUT, HWMON_F_INPUT),
	NULL
};

static const struct hwmon_ops asetekpro_hwmon_ops = {
	.is_visible = asetekpro_hwmon_is_visible,
	.read = asetekpro_hwmon_read,
};

static const struct hwmon_chip_info asetekpro_chip_info = {
	.ops = &asetekpro_hwmon_ops,
	.info = asetekpro_hwmon_info,
};

static int asetekpro_probe(struct usb_interface *intf, const struct usb_device_id *id)
{
	struct usb_device *udev = interface_to_usbdev(intf);
	struct usb_endpoint_descriptor *bulk_in, *bulk_out;
	struct device *dev = &intf->dev;
	struct asetekpro_device *adev;
	int ret;

	adev = devm_kzalloc(dev, sizeof(*adev), GFP_KERNEL);
	if (!adev)
		return -ENOMEM;

	adev->buf = devm_kmalloc(dev, 8, GFP_KERNEL);
	if (!adev->buf)
		return -ENOMEM;

	adev->dev = dev;
	adev->udev = usb_get_dev(udev);
	usb_set_intfdata(intf, adev);
	dev_info(dev, "Probing device: %s %s\n", udev->manufacturer, udev->product);

	ret = usb_find_common_endpoints(intf->cur_altsetting, &bulk_in, &bulk_out, NULL, NULL);
	if (ret) {
		dev_err(dev, "Could not find bulk in/out endpoints!\n");
		kfree(adev->buf);
		return ret;
	}

	adev->in_addr = bulk_in->bEndpointAddress;
	adev->out_addr = bulk_out->bEndpointAddress;

	ret = asetekpro_control_msg(adev, USBXPRESS_UNLOCK, 0);
	if (ret)
		return ret;

	adev->hdev = devm_hwmon_device_register_with_info(dev, KBUILD_MODNAME, adev, &asetekpro_chip_info, NULL);
	return PTR_ERR_OR_ZERO(adev->hdev);
}

static void asetekpro_disconnect(struct usb_interface *intf)
{
	struct asetekpro_device *adev = usb_get_intfdata(intf);
	struct usb_device *udev = adev->udev;
	struct device *dev = adev->dev;

	dev_info(dev, "Removing device: %s %s\n", udev->manufacturer, udev->product);
	asetekpro_control_msg(adev, USBXPRESS_LOCK, 200);
	usb_set_intfdata(intf, NULL);
	usb_put_dev(udev);
}

static struct attribute *asetekpro_attrs[] = {
	&dev_attr_fixed.attr,
	&dev_attr_shift.attr,
	&dev_attr_version.attr,
	&dev_attr_pump_mode.attr,
	NULL
};
ATTRIBUTE_GROUPS(asetekpro);

static struct usb_driver asetekpro_driver = {
	.name =		KBUILD_MODNAME,
	.probe =	asetekpro_probe,
	.disconnect =	asetekpro_disconnect,
	.id_table =	id_table,
	.dev_groups =	asetekpro_groups,
};
module_usb_driver(asetekpro_driver);

MODULE_AUTHOR("Harry Austen <harryausten@hotmail.co.uk>");
MODULE_DESCRIPTION("Corsair asetekpro-based liquid cooler driver");
MODULE_LICENSE("GPL");
