// SPDX-License-Identifier: MIT
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/socket.h>
#include <linux/if_alg.h>
#include <sys/param.h>
#include <string.h>
#include <strings.h>
#include <time.h>

static const uint32_t poly = 0x82F63B78;
//static const uint32_t poly = 0xEDB88320;

#define CRC_ALG "crc32c"
//#define CRC_ALG "crc32"

int kernel_crc(const void *data, size_t len)
{
	int crc32c;
	int sds[2] = { -1, -1 };
	struct sockaddr_alg sa = {
		.salg_family = AF_ALG,
		.salg_type   = "hash",
		.salg_name   = CRC_ALG
	};

	sds[0] = socket(AF_ALG, SOCK_SEQPACKET, 0);
	if (sds[0] == -1) {
		printf("socket returned: %s\n", strerror(errno));
		return -1;
	}

	if (bind(sds[0], (struct sockaddr *) &sa, sizeof(sa))) {
		printf("bind returned: %s\n", strerror(errno));
		return -1;
	}

	sds[1] = accept(sds[0], NULL, NULL);
	if (sds[1] == -1) {
		printf("accept returned: %s\n", strerror(errno));
		return -1;
	}

	if (send(sds[1], data, len, MSG_MORE) != (ssize_t)len) {
		printf("send returned: %s\n", strerror(errno));
		return -1;
	}

	if (read(sds[1], &crc32c, sizeof(crc32c)) != sizeof(crc32c)) {
		printf("read returned: %s\n", strerror(errno));
		return -1;
	}

	close(sds[1]);
	close(sds[0]);
	return crc32c;
}

int userspace_crc(const void *data, size_t len)
{
	uint32_t crc = 0xFFFFFFFF;
	const uint8_t *current = (const uint8_t *) data;

	while (len--) {
		crc ^= *current++;
		for (uint8_t j = 0; j < 8; j++)
			crc = (crc >> 1) ^ (-(int)(crc & 1) & poly);
	}
	return (int)~crc;
}

void run_algorithm(int (*f) (const void *, size_t),
		   const void *data, const size_t len)
{
	const clock_t start = clock();
	const int ret = f(data, len);
	const clock_t stop = clock();
	const double elapsed_ms =
		(double)(stop - start) * 1000.0 / CLOCKS_PER_SEC;

	printf("val = %08X, time = %fms\n", ret, elapsed_ms);
}

static const char charset[] =
	"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
static const int num_chars = sizeof(charset) - 1;

void rand_str(char * const s, const size_t n)
{
	for (size_t i = 0; i < n; ++i)
		s[i] = charset[rand() % num_chars];
}

int main(void)
{
	const size_t len = 1<<20;
	char data[len];

	rand_str(data, len);
	printf("Generated %lu length string to test CRC algorithms\n\n", len);

	puts("kernel:");
	run_algorithm(&kernel_crc, data, len);
	puts("userspace:");
	run_algorithm(&userspace_crc, data, len);
	return 0;
}
