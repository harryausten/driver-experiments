# Location of kernel build directory
# (default to install location for currently running kernel)
KERNELDIR ?= /lib/modules/$(shell uname -r)/build
# Kernel sparse check level (default 1 - check on compile)
C ?= 1
# Kernel compile warning level (default 1 - any higher errors due to in-tree issues)
W ?= 1
# List of non-kernel C programs
PROGS := crc32 lsmod
# Compile flags for programs
CFLAGS := -Og -g
# Link flags for programs
LDFLAGS := -lkmod
# Needed when using sudo
PWD ?= $(shell pwd)

# Default to comiling kernel modules and userspace progs
.PHONY: all
all: compile_commands.json modules progs

# Clean target also removes compiled progs
.PHONY: clean
clean:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) clean
	$(RM) $(PROGS)

compile_commands.json:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) C=$(C) W=$(W) LLVM=1 $@

# Redirect kernel related targets to the kernel makefile
.PHONY: modules modules_install
modules modules_install:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) C=$(C) W=$(W) LLVM=1 $@

# Helper target for compiling all progs
.PHONY: progs
progs: $(PROGS)
