// SPDX-License-Identifier: GPL-2.0
// For module_init, module_exit and MODULE_LICENSE macros
#include <linux/module.h>
// For printk and levels
#include <linux/printk.h>
// For inode, file and file_operations structs
#include <linux/fs.h>
// Simple cdev implementation and helper funcs to reduce boilerplate
#include <linux/miscdevice.h>
// For copy_to_user and copy_from_user
#include <linux/uaccess.h>
// For mutexes
#include <linux/mutex.h>

// Character buffer in kernelspace
// Accessible from userspace using this driver's read/write funcs
#define KERN_BUF_LEN 100
static char kern_buf[KERN_BUF_LEN] = "Hello World\n";

// Mutexes
static DEFINE_MUTEX(access_mutex);
static bool is_cdev_open;
static DEFINE_MUTEX(transfer_mutex);

// Simple open implementation
static int myopen(struct inode *inode, struct file *filp)
{
	int ret;

	pr_info("cdev OPEN\n");
	mutex_lock(&access_mutex);

	if (is_cdev_open) {
		pr_err("cdev Failed to acquire access mutex\n");
		ret = -EWOULDBLOCK;
	} else {
		is_cdev_open = true;
		ret = 0;
	}

	mutex_unlock(&access_mutex);
	return ret;
}

// Simple close implementation
static int myrelease(struct inode *inode, struct file *filp)
{
	pr_info("cdev CLOSE\n");
	mutex_lock(&access_mutex);
	is_cdev_open = false;
	mutex_unlock(&access_mutex);

	return 0;
}

// Read from the kernelspace string
static ssize_t myread(struct file *filp, char __user *buff, size_t count, loff_t *offp)
{
	size_t err;
	size_t len = min((size_t)(strlen(kern_buf) - *offp), count);

	pr_info("cdev READ (count = %lu, off = %lld, len = %lu)\n",
	       count, *offp, len);

	if (len <= 0)
		return 0;

	// Lock transfer_mutex
	if (mutex_lock_interruptible(&transfer_mutex)) {
		pr_err("cdev Failed to acquire transfer mutex\n");
		return -EWOULDBLOCK;
	}

	// Copy userspace memory into kernelspace
	err = copy_to_user(buff, kern_buf + *offp, len);

	// Release transfer mutex
	mutex_unlock(&transfer_mutex);

	if (err) {
		pr_err("cdev copy_to_user failed to copy %lu bytes\n", err);
		return -EFAULT;
	}

	*offp += len;
	return len;
}

// Write to the kernelspace string
static ssize_t mywrite(struct file *filp, const char __user *usr_buf,
		size_t count, loff_t *offp)
{
	size_t err;
	size_t len = min((size_t)(KERN_BUF_LEN - *offp), count);

	pr_info("cdev WRITE (count = %lu, off = %lld, len = %lu)\n",
	       count, *offp, len);

	if (len <= 0)
		return 0;

	// Lock transfer_mutex
	if (mutex_lock_interruptible(&transfer_mutex)) {
		pr_err("cdev Failed to acquire transfer mutex\n");
		return -EWOULDBLOCK;
	}

	// Copy kernelspace memory into userspace
	err = copy_from_user(kern_buf + *offp, usr_buf, len);

	// Release transfer mutex
	mutex_unlock(&transfer_mutex);

	if (err) {
		pr_err("cdev copy_from_user failed to copy %lu bytes\n", err);
		return -EFAULT;
	}

	kern_buf[len + *offp] = 0;
	pr_info("cdev WRITE: %s\n", kern_buf);

	*offp += len;
	return len;
}

// Define file operation functions for cdev
static const struct file_operations mycdev_fops = {
	.owner = THIS_MODULE,
	.read = myread,
	.write = mywrite,
	.open = myopen,
	.release = myrelease
};

// Use misc device to reduce boilerplate code
static struct miscdevice mycdev_miscdevice = {
	.name	= "mycdev",
	.fops	= &mycdev_fops,
	.minor	= MISC_DYNAMIC_MINOR,
};
module_misc_device(mycdev_miscdevice);

// Kernel module macros
MODULE_AUTHOR("Harry Austen <harryausten@hotmail.co.uk>");
MODULE_DESCRIPTION("Test character device driver");
MODULE_LICENSE("GPL");
