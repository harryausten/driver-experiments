// SPDX-License-Identifier: MIT
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <libkmod.h>

int main(void)
{
	const char *cfg;
	struct kmod_ctx *ctx;
	struct kmod_list *lst, *elem;
	int ret;

	ctx = kmod_new(NULL, &cfg);
	ret = kmod_module_new_from_loaded(ctx, &lst);
	if (ret) {
		printf("Failed to get module list! %s\n", strerror(errno));
		goto exit;
	}

	puts("Module              |     Size | Dependencies");
	puts("----------------------------------------------------------------");
	kmod_list_foreach(elem, lst) {
		struct kmod_module *mod = kmod_module_get_module(elem);
		const char * const options = kmod_module_get_options(mod);
		struct kmod_list *deps = kmod_module_get_dependencies(mod);
		struct kmod_list *entry;
		struct kmod_module *mod_dep;

		printf("%-19s | %8ld | ",
			kmod_module_get_name(mod),
			kmod_module_get_size(mod));
		kmod_list_foreach(entry, deps) {
			mod_dep = kmod_module_get_module(entry);
			printf("%s", kmod_module_get_name(mod_dep));
			if (kmod_list_next(deps, entry))
				putchar(',');
			kmod_module_unref(mod_dep);
		}
		putchar('\n');
		if (options)
			printf("\t%s\n", options);
		kmod_module_unref_list(deps);
		kmod_module_unref(mod);
	}

exit:
	kmod_module_unref_list(lst);
	kmod_unref(ctx);
	return ret;
}
